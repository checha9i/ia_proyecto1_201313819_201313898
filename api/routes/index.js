var express = require("express");
var router = express.Router();
var prolog = require("../prolog");
/* GET home page. */
router.get("/", function (req, res, next) {
  var salida = prolog.prueba("america(X).");

  setTimeout(() => {
    res.send(salida);
  }, 2000);
});

module.exports = router;
