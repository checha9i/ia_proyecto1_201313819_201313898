/*prolog*/

var pl = require("tau-prolog/modules/core");

// Import the lists module
// Looks like this when tau-prolog is installed with npm:
// require("tau-prolog/modules/lists")(pl);
require("tau-prolog/modules/lists")(pl);
// Create a session
var session = pl.create(1000);
const fs = require("fs");

const prueba = (query) => {
  // Load the program
  var text = fs.readFileSync("./Base de conocimiento.pl", "utf8");
  var salida = [];
  // Load the program
  text += "\n" + query;
  session.consult(text, {
    success: function () {
      // Query the goal
      session.query("buscar(X).", {
        success: function () {
          // Look for answers
          session.answers(show(salida), 1000);

          setTimeout(() => {
            //console.log(salida);
            return salida;
          }, 1000);
        },
      });
    },
  });
  return salida;
};

const show = (salida) => {
  // Get output container

  // Return callback function
  return function (answer) {
    // Valid answer
    if (pl.type.is_substitution(answer)) {
      // Get the value of the food
      // console.log(pl.format_answer(answer));
      // Get the person
      //var ans = pl.format_answer(answer).replace(/\=/g, ":").replace(";", "");
      var salida2 = answer.lookup("X");
      //salida.push(ans);
      //console.log(ans);

      salida.push(salida2.id);
      // Show answer
    }
  };
};

module.exports = {
  prueba: prueba,
  show: show,
};
