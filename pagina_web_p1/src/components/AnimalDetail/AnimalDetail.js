import React from "react";
import "./AnimalDetail.css";
import {
  Container,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  Row,
  Col,
  Table,
} from "reactstrap";

import { FadeTransform } from "react-animation-components";

const AnimalDetail = ({ animal }) => {
  if (animal === undefined)
    return (
      <FadeTransform
        in
        transformProps={{
          exitTransform: "scale(0.5) translateY(-50%)",
        }}
      >
        <div>No existe este animal</div>
      </FadeTransform>
    );
  else
    return (
      <Container fluid>
        <Row style={{ marginTop: 100 }}>
          <Col className="offset-1 col-10">
            <FadeTransform
              in
              transformProps={{
                exitTransform: "scale(0.5) translateY(-50%)",
              }}
            >
              <Card>
                <Row>
                  <Col>
                    <CardImg
                      src={animal.url}
                      alt={animal.nombre}
                      style={{ "margin-top": 75 }}
                    />
                  </Col>
                  <Col>
                    <CardBody>
                      <CardTitle>
                        <h1 className="offset-md-3">{animal.nombre}</h1>
                      </CardTitle>
                      <CardText>
                        <Table responsive striped>
                          <tbody>
                            <tr>
                              <th scope="row">Nombre Cientifico</th>
                              <td>{animal.cientifico}</td>
                            </tr>
                            <tr>
                              <th scope="row">Peso Máximo</th>
                              <td>{animal.peso}</td>
                            </tr>
                            <tr>
                              <th scope="row">Longitud Máxima</th>
                              <td>{animal.tamaño}</td>
                            </tr>
                            <tr>
                              <th scope="row">Años de vida</th>
                              <td>{animal.años}</td>
                            </tr>
                            <tr>
                              <th scope="row">Continentes</th>
                              <td>{animal.continente}</td>
                            </tr>
                            <tr>
                              <th scope="row">Velocidad</th>
                              <td>{animal.velocidad}</td>
                            </tr>
                            <tr>
                              <th scope="row">Población Salvaje</th>
                              <td>{animal.liberados}</td>
                            </tr>
                          </tbody>
                        </Table>
                      </CardText>
                    </CardBody>
                  </Col>
                </Row>
              </Card>
            </FadeTransform>
          </Col>
        </Row>
      </Container>
    );
};

export default AnimalDetail;
