import React, { Component } from "react";
import "./Main.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "../Home/Home";
import AnimalDetail from "../AnimalDetail/AnimalDetail";
import axios from "axios";
import Header from "../Header/Header";
import { serverURL } from "../../shared/baseURL";
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animals: [],
      caracteristicas: [],
    };
  }

  componentDidMount() {
    axios.get(serverURL + "animals/caracteristicas").then((res) => {
      this.setState({ caracteristicas: res.data });
      //console.log(res.data);
    });
    axios.get(serverURL + "animals").then((res) => {
      this.setState({ animals: res.data });
      //console.log(res.data);
    });
  }

  render() {
    const AnimalWithID = ({ match }) => {
      return (
        <AnimalDetail
          animal={
            this.state.animals.filter(
              (animal) => animal._id === match.params.animalId
            )[0]
          }
        />
      );
    };

    return (
      <div>
        <Header />
        <Router>
          <Switch>
            <Route path="/animal/:animalId" component={AnimalWithID} />
            <Route path="/">
              <Home
                caracteristicas={this.state.caracteristicas}
                animals={this.state.animals}
              />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Main;
