import React, { Component } from "react";

import "./Home.css";
import {
  Col,
  Row,
  Container,
  Form,
  Input,
  FormGroup,
  Label,
  Button,
  Table,
} from "reactstrap";
import TagsInput from "react-tagsinput";
import { Link } from "react-router-dom";
import { serverURL } from "../../shared/baseURL";
import axios from "axios";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCaracteristics: [],
      fields: {},
      tags: [],
      animal: "",
      res: "",
      filtrado: [],
    };
    this.onSelect = this.onSelect.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onChangeAnimal = this.onChangeAnimal.bind(this);
  }

  handleChange(tags) {
    this.setState({ tags });
  }

  componentDidMount() {
    setTimeout(() => {
      //console.log(this.props.animals);
      this.setState({ filtrado: this.props.animals });
    }, 500);
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.state.tags.length === 0) {
      alert("Seleccione al menos 1 caracteristica");
    } else if (this.state.animal === "") {
      alert("Ingrese el nombre del animal");
    } else {
      var body = {
        tags: this.state.tags,
        animal: this.state.animal.toLowerCase(),
      };
      axios.post(serverURL + "animals/exist", body).then((res) => {
        this.setState({ res: res.data });
      });
    }
  }

  onChangeAnimal(e) {
    //console.log(e.target.value);
    this.setState({ animal: e.target.value, res: "" });
  }

  onSelect(e) {
    var fields = this.state.fields;
    fields[e.target.name] = !this.state.fields[e.target.name];
    var tags = this.state.tags;

    const caracIndex = this.state.tags.findIndex(
      (element) => element === e.target.name
    );

    if (caracIndex === -1) tags.push(e.target.name);
    else {
      tags.splice(caracIndex, 1);
    }

    if (tags.length === 0) {
      this.setState({ filtrado: this.props.animals });
    } else {
      var body = { tags: tags };
      axios.post(serverURL + "animals/filtrado", body).then((res) => {
        //this.setState({ filtrado: res.data });
        var filtrado = [];

        res.data.forEach((animal) => {
          var filt = this.props.animals.filter(
            (anim) => anim.nombre === animal
          );
          filt.forEach((element) => {
            filtrado.push(element);
          });
        });
        this.setState({ filtrado: filtrado });
      });
    }

    this.setState({
      fields: fields,
      tags: tags,
      res: "",
    });
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col className="offset-md-4">
            <h1>Zoologico La Aurora</h1>
          </Col>
        </Row>

        <Row>
          <Col className="col-3" style={{}}>
            <Container fluid>
              <h3>Caracteristicas</h3>
              <TagsInput
                value={this.state.tags}
                onChange={this.handleChange}
                disabled
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "",
                }}
              />

              {this.props.caracteristicas.map((element, key) => {
                return (
                  <FormGroup check key={key}>
                    <Label>
                      <Input
                        checked={this.state.fields[element.value]}
                        onClick={this.onSelect}
                        type="checkbox"
                        name={element.value}
                        id={element.value}
                        value={element.value}
                      />
                      {element.value}
                    </Label>
                  </FormGroup>
                );
              })}
            </Container>
          </Col>

          <Col>
            <Form inline className="m-4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="animalName" className="mr-sm-2">
                  El
                </Label>
                <Input
                  type="text"
                  name="animalName"
                  id="animalName"
                  placeholder="leopardo"
                  onChange={this.onChangeAnimal}
                  value={this.state.animal}
                />
              </FormGroup>
              <FormGroup md={4}>
                <Label>posee esas caracteristicas</Label>
              </FormGroup>

              <Button outline className="offset-sm-1" onClick={this.onSubmit}>
                Consultar
              </Button>
            </Form>
            <p>{this.state.res}</p>
            <Table responsive striped>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Nombre Cientifico</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.filtrado.map((animal, key) => {
                  return (
                    <tr key={key}>
                      <td>{animal.nombre}</td>
                      <td>{animal.cientifico}</td>
                      <td>
                        <Link to={"/animal/" + animal._id}>Info</Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;
